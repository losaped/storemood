package main

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
)

const (
	bgPath               = "./bg.jpg"
	wmPath               = "./watermark.png"
	watermarkRequestPath = "http://localhost:3210/watermark"
)

func main() {

	var b bytes.Buffer
	w := multipart.NewWriter(&b)
	defer w.Close()

	if err := createFormFile(w, bgPath, "background"); err != nil {
		log.Fatal(err)
	}

	if err := createFormFile(w, wmPath, "watermark"); err != nil {
		log.Fatal(err)
	}

	w.Close()

	req, err := http.NewRequest(http.MethodPost, watermarkRequestPath, &b)
	if err != nil {
		log.Fatalf("error on create request: %v", err)
	}

	req.Header.Set("Content-Type", w.FormDataContentType())

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatalf("error on send request: %v", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		bts, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(bts))
		log.Fatalf("bad response status: %d. Error: %s", resp.StatusCode, resp.Status)
	}

	resultFile, err := os.Create("./watermark.jpg")
	if err != nil {
		log.Fatalf("error on create result file: %v", err)
	}

	defer resultFile.Close()

	if _, err := io.Copy(resultFile, resp.Body); err != nil {
		log.Fatalf("error on saving result file: %v", err)
	}

	log.Println("watermarked image is saved")
}

func createFormFile(formWriter *multipart.Writer, filePath, fieldName string) error {
	bgFile, err := os.Open(filePath)
	if err != nil {
		return fmt.Errorf("error on open background file: %v", err)
	}
	defer bgFile.Close()

	_, bgFileName := filepath.Split(filePath)
	fmt.Printf("sending file: %s with fieldName: %s\n", bgFileName, fieldName)
	fw, err := formWriter.CreateFormFile(fieldName, bgFileName)
	if err != nil {
		return fmt.Errorf("error on creating form background field: %v", err)
	}

	if _, err := io.Copy(fw, bgFile); err != nil {
		return fmt.Errorf("error on loading background: %v", err)
	}

	return nil
}
