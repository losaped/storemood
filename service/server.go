package main

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"log"
	"net/http"

	img "bitbucket.org/losaped/storemood/image"
	"bitbucket.org/losaped/storemood/service/config"
)

const (
	backgroundField       = "background"
	watermarkField        = "watermark"
	mimePng               = "image/png"
	mimeJpg               = "image/jpeg"
	maxFileSize           = 10 << 20
	contentTypeHeaderSize = 512
)

type ErrorContentType struct {
	ContentType string
}

func (err ErrorContentType) Error() string {
	return "wrong content type: " + err.ContentType
}

func main() {
	http.HandleFunc("/watermark", watermarkHandler)
	fmt.Println("server running on", config.GetPort())
	http.ListenAndServe(config.GetPort(), nil)
}

func watermarkHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		http.Error(w, "wrong method", http.StatusBadRequest)
		return
	}

	// загрузим фон
	bg, err := uploadImage(w, r, backgroundField, mimeJpg)
	if err != nil {
		log.Printf("on load background: %v", err)
		return
	}

	// загрузим картинку для водяного знака
	watermark, err := uploadImage(w, r, watermarkField, mimePng)
	if err != nil {
		log.Printf("on load watermark: %v", err)
		return
	}

	// сделаем ресайз по условию, можем добавить любую функциональность для нормализации
	bg = img.NormalizeClientImage(bg)

	// Замостим водяным знаком фон,
	// TileCentralAlignment позволяет сделать выравнивание по центру
	// если нужно другое выравнивание, то нужно только добавить реализацию
	bg = img.Tile(bg, watermark, img.TileCentralAlignment{})

	w.Header().Set("Content-Type", mimeJpg)

	if err := encodeJPEG(bg, w); err != nil {
		log.Printf("on sending image: %v", err)
	}
}

func uploadImage(w http.ResponseWriter, r *http.Request, fieldName, contentType string) (image.Image, error) {
	f, h, err := r.FormFile(fieldName)
	if err != nil {
		fmt.Println("ERROR", err)
		if err == http.ErrMissingFile {
			http.Error(w, fieldName+" file is missing", http.StatusBadRequest)
			return nil, err
		}

		http.Error(w, "error on loading "+fieldName, http.StatusInternalServerError)
		return nil, err
	}

	if h.Size > maxFileSize {
		http.Error(w, "max file size is 10 MB", http.StatusBadRequest)
		return nil, err
	}

	log.Println("header file size:", h.Size)
	defer f.Close()

	fmt.Printf("fieldName: %s, contentType %s\n", fieldName, contentType)
	buf := make([]byte, h.Size)
	if _, err := f.Read(buf[:contentTypeHeaderSize]); err != nil {
		http.Error(w, "error on checking file", http.StatusBadRequest)
		return nil, err
	}

	// если contentType не соответствует требуемому, то вернем ошибку
	ct := http.DetectContentType(buf)
	if ct != contentType {
		http.Error(w, "wrong content type", http.StatusBadRequest)
		return nil, ErrorContentType{ContentType: ct}
	}

	if _, err := f.Read(buf[contentTypeHeaderSize:]); err != nil {
		http.Error(w, "error file upload", http.StatusInternalServerError)
		return nil, err
	}

	fileBuf := bytes.NewBuffer(buf)
	img, _, err := image.Decode(fileBuf)
	if err != nil {
		http.Error(w, "error on decoding image", http.StatusInternalServerError)
		return nil, err
	}

	return img, nil
}

func encodeJPEG(picture image.Image, w io.Writer) error {
	return jpeg.Encode(w, picture, &jpeg.Options{
		Quality: jpeg.DefaultQuality,
	})
}

func encodePNG(picture image.Image, w io.Writer) error {
	return png.Encode(w, picture)
}
