package config

import "os"

const (
	portEnv = "PORT"
)

func GetPort() string {
	port := os.Getenv(portEnv)
	if port == "" {
		port = ":3210"
	}

	return port
}
