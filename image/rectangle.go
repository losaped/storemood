package image

import (
	"fmt"
	"image"
)

// ContentLayerBounder интерфейс для формирования области контента относительно фона
type ContentLayerBounder interface {
	GetContentBounds(bg, content image.Rectangle) image.Rectangle
}

// TileCentralAlignment умеет возвращать область для замощения с совмещением
// фона и плитки
type TileCentralAlignment struct {
}

func (tca TileCentralAlignment) GetContentBounds(bg, content image.Rectangle) image.Rectangle {
	xCount := bg.Size().X / content.Size().X
	if xCount%2 == 0 {
		xCount++
	} else {
		xCount += 2
	}

	yCount := bg.Size().Y / content.Size().Y
	if yCount%2 == 0 {
		yCount++
	} else {
		yCount += 2
	}

	contentLayerLength := content.Size().X * xCount
	contentLayerHeight := content.Size().Y * yCount

	contentLayerRect := image.Rectangle{
		Min: bg.Bounds().Min,
		Max: bg.Bounds().Min.Add(image.Pt(contentLayerLength, contentLayerHeight)),
	}

	offset := contentLayerRect.Max.Sub(bg.Bounds().Max).Div(2)
	return contentLayerRect.Sub(offset)
}

// Tile может замостить фон (bg) вотермарками (content) выбрав выравнивание с помощью lBounder
func Tile(bg, content image.Image, lBounder ContentLayerBounder) image.Image { //image.Image

	maskRect := lBounder.GetContentBounds(bg.Bounds(), content.Bounds())
	fmt.Println(maskRect)

	colCount := maskRect.Size().X / content.Bounds().Size().X
	rowCount := maskRect.Size().Y / content.Bounds().Size().Y

	for row := 1; row <= rowCount; row++ {

		maxY := maskRect.Bounds().Min.Y + content.Bounds().Size().Y*row
		minY := maxY - content.Bounds().Size().Y

		for col := 1; col <= colCount; col++ {
			maxX := maskRect.Bounds().Min.X + content.Bounds().Size().X*col
			minX := maxX - content.Bounds().Size().X

			rect := image.Rect(minX, minY, maxX, maxY)

			bg = Watermark(bg, content, rect, 70)
		}
	}

	return bg
}
