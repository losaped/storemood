package image

import (
	"image"
	"image/color"
	"image/draw"
	"math"

	"github.com/nfnt/resize"
)

func NormalizeClientImage(img image.Image) image.Image {
	return resize.Thumbnail(1024, 768, img, resize.Lanczos2)
}

// Watermark рисует вотермарк (wm) на фоне (bg) в указанных границах (rTarget) с прозрачностью
// заданной в процентах
func Watermark(bg, wm image.Image, rTarget image.Rectangle, opacity int) image.Image {
	m := image.NewRGBA(bg.Bounds())
	draw.Draw(
		m,
		bg.Bounds(),
		bg,
		image.ZP,
		draw.Src)

	draw.DrawMask(
		m,
		rTarget,
		wm,
		image.ZP,
		&image.Uniform{color.Alpha{opacityFromPerc(opacity)}},
		image.ZP,
		draw.Over)

	return m
}

func opacityFromPerc(perc int) uint8 {
	invert := float64(100 - perc)
	return uint8(math.Round(float64(255) / 100 * invert))
}
